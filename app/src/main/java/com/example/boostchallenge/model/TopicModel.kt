package com.example.boostchallenge.model

data class TopicModel (
        var topicUID: Long = System.currentTimeMillis(), // unique id of topic, generated on creation using timestamp
        var topicVotes: Int = 0, // number of votes, can be negative
        var topicTitle: String // topic title as entered during creation
)