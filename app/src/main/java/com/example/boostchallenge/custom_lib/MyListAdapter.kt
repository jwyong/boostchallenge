package com.example.boostchallenge.custom_lib

import androidx.recyclerview.widget.*

abstract class MyListAdapter<T, VH : RecyclerView.ViewHolder> : RecyclerView.Adapter<VH> {
    private val mHelper: AsyncListDiffer<T>

    protected constructor(diffCallback: DiffUtil.ItemCallback<T>) {
        mHelper = AsyncListDiffer(AdapterListUpdateCallback(this),
                AsyncDifferConfig.Builder(diffCallback).build())
    }

    protected constructor(config: AsyncDifferConfig<T>) {
        mHelper = AsyncListDiffer(AdapterListUpdateCallback(this), config)
    }

    /**
     * Submits a new list to be diffed, and displayed.
     *
     *
     * If a list is already being displayed, a diff will be computed on a background thread, which
     * will dispatch Adapter.notifyItem events on the main thread.
     *
     * @param list The new list to be displayed.
     */
    fun submitList(list: List<T>?): Boolean {
        if (mHelper.currentList.size < list!!.size) { // newList more than oldList, item added
            mHelper.submitList(list)
            return true

        } else { // same size, no need scroll
            mHelper.submitList(null) // submit null first for diffUtil bug
            mHelper.submitList(list)
            return false

        }

    }

    protected fun getItem(position: Int): T {
        return mHelper.currentList[position]
    }

    override fun getItemCount(): Int {
        return mHelper.currentList.size
    }
}