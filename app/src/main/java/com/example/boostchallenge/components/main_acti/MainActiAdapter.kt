package com.example.boostchallenge.components.main_acti

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.boostchallenge.databinding.TopicListItemBinding
import androidx.databinding.DataBindingUtil
import com.example.boostchallenge.model.TopicModel
import com.example.boostchallenge.R
import com.example.boostchallenge.custom_lib.MyListAdapter
import com.example.boostchallenge.helpers.GlobalVars

class MainActiAdapter(private val vm: MainActiVM) : MyListAdapter<TopicModel, MainActiAdapter.Holder>(
        DIFF_CALLBACK
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
                DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.topic_list_item,
                        parent,
                        false
                ) as TopicListItemBinding
        )
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        Log.d(GlobalVars.TAG1, "MainActiAdapter: onBindViewHolder item = ${getItem(position)}")

        holder.setData(getItem(position), vm)
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).topicUID
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<TopicModel>() {
            override fun areItemsTheSame(
                    oldItem: TopicModel, newItem: TopicModel
            ): Boolean {
                return oldItem.topicUID == newItem.topicUID && oldItem.topicVotes == newItem.topicVotes
            }

            override fun areContentsTheSame(
                    oldItem: TopicModel, newItem: TopicModel
            ): Boolean {
//                return false
                return oldItem == newItem
            }
        }
    }

    class Holder(private val itemBinding: TopicListItemBinding) :
            RecyclerView.ViewHolder(itemBinding.root) {
        fun setData(item: TopicModel, vm: MainActiVM) {
            itemBinding.data = item

            // set onclick for up/down btns
            itemBinding.btnDown.setOnClickListener {
                vm.upDownVoteBtn(0, item)
            }

            itemBinding.btnUp.setOnClickListener {
                vm.upDownVoteBtn(1, item)
            }

            // set onclick for navigate to topic details
            itemView.setOnClickListener {
                vm.topicItemOnClick(itemView.context, item)
            }
        }
    }

}