package com.example.boostchallenge.components.main_acti

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.boostchallenge.helpers.GlobalVars
import com.example.boostchallenge.helpers.UIHelper
import com.example.boostchallenge.R
import com.example.boostchallenge.components.topic_acti.TopicActi
import com.example.boostchallenge.model.TopicModel
import dagger.Component

class MainActiVM : ViewModel() {
    // helpers
    private val uiHelper = UIHelper()

    // for rv
    val topicListLD = MutableLiveData<MutableList<TopicModel>>(mutableListOf())
    private lateinit var rv: RecyclerView
    val rvAdapter = MainActiAdapter(this)

    //===== databinding funcs
    // create new topic onclick
    fun addBtnOnClick(v: View) {
        // prep func for topic creation confirmation
        val creationFunc = Runnable {
            Log.d(GlobalVars.TAG1, "MainActiVM: addBtnOnClick inputText = ${uiHelper.inputText}")

            // add item to list
            topicListLD.add(
                    TopicModel(
                            topicTitle = uiHelper.inputText!!
                    )
            )
        }

        // show popup for user to create new topic
        uiHelper.dialog2btn(
                context = v.context,
                message = R.string.topic_create_msg,
                positiveFunc = creationFunc
        )
    }

    // upvote/downvote func
    fun upDownVoteBtn(type: Int, item: TopicModel) {
        Log.d(GlobalVars.TAG1, "MainActiVM: upDownVoteBtn item = $item")

        // remove selected item from list
        topicListLD.value?.remove(item)

        // add new item with updated vote
        if (type == 0) { // down
            item.topicVotes = item.topicVotes - 1
        } else { // up
            item.topicVotes = item.topicVotes + 1
        }
        Log.d(GlobalVars.TAG1, "MainActiVM: upDownVoteBtn item = $item")

        topicListLD.add(item)
    }

    // topic item on click - go to new activity
    fun topicItemOnClick(context: Context, item: TopicModel) {
        Log.d(GlobalVars.TAG1, "MainActiVM: topicItemOnClick ")

        // go to topic details acti
        val intent = Intent(context, TopicActi::class.java)
        intent.putExtra(GlobalVars.EX_TOPIC_TITLE, item.topicTitle)
        intent.putExtra(GlobalVars.EX_TOPIC_VOTES, item.topicVotes)
        context.startActivity(intent)
    }

    //===== normal funcs
    fun setupRV(recyclerView: RecyclerView) {
        rv = recyclerView
        rv.apply {
            layoutManager = LinearLayoutManager(context)
            rvAdapter.setHasStableIds(true)
            adapter = rvAdapter
        }
    }

    fun setupLiveData(lifecycleOwner: LifecycleOwner) {
        topicListLD.observe(lifecycleOwner, Observer { list ->
            // sort newList by votes first (desc)
            var newList = list.sortedWith(compareByDescending { it.topicVotes })
            Log.d(GlobalVars.TAG1, "MainActiVM: setupLiveData newlist size = ${newList.size}")

            // show first 20 items only
            newList = newList.take(20)
            Log.d(GlobalVars.TAG1, "MainActiVM: setupLiveData newlist size = ${newList.size}")

            // save rv scroll state
            val recyclerViewState = rv.layoutManager?.onSaveInstanceState()

            // submit new list to adapter for ui updating
            if (rvAdapter.submitList(newList) && !newList.isNullOrEmpty()) {
                // scroll to btm ONLY if item added to list
                rv.smoothScrollToPosition(newList.size - 1)

            } else { // item NOT added, remain state
                rv.layoutManager?.onRestoreInstanceState(recyclerViewState)
            }
        })
    }

    // update item to liveData list and notify
    fun <T> MutableLiveData<MutableList<T>>.add(item: T) {
        val updatedItems = this.value as ArrayList
        updatedItems.add(item)
        this.value = updatedItems
    }
}