package com.example.boostchallenge.components.common

import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.boostchallenge.R

open class BaseActi : AppCompatActivity() {
    protected fun setupToolbar(titleStrID: Int) {
        val mToolbar: Toolbar = findViewById(R.id.toolbar)

        setSupportActionBar(mToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.title = getString(titleStrID)

        mToolbar.setNavigationOnClickListener { onBackPressed() }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}