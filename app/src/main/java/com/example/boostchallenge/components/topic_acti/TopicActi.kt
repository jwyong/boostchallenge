package com.example.boostchallenge.components.topic_acti

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.boostchallenge.R
import com.example.boostchallenge.components.common.BaseActi
import com.example.boostchallenge.components.main_acti.MainActi
import com.example.boostchallenge.components.main_acti.MainActiVM
import com.example.boostchallenge.databinding.TopicActiBinding
import com.example.boostchallenge.helpers.GlobalVars
import com.example.boostchallenge.model.TopicModel
import javax.inject.Inject

class TopicActi : BaseActi() {
    private lateinit var binding: TopicActiBinding

    // vars for data
    lateinit var topicTitle: String
    lateinit var topicVotes: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this,
                R.layout.topic_acti
        )
        binding.vm = this

        // setup toolbar with title and back btn
        setupToolbar(R.string.topic_dets)

        setupData()
    }

    private fun setupData() {
        topicTitle = intent.getStringExtra(GlobalVars.EX_TOPIC_TITLE)?: ""
        topicVotes = intent.getIntExtra(GlobalVars.EX_TOPIC_VOTES, 0).toString()
    }
}
