package com.example.boostchallenge.components.main_acti

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.boostchallenge.R
import com.example.boostchallenge.databinding.MainActiBinding
import kotlinx.android.synthetic.main.main_acti.*

class MainActi : AppCompatActivity() {
    private lateinit var binding: MainActiBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this,
            R.layout.main_acti
        )
        binding.lifecycleOwner = this

        setSupportActionBar(toolbar)

        setupVM()
    }

    private fun setupVM() {
        val vm = ViewModelProviders.of(this).get(MainActiVM::class.java)
        binding.vm = vm

        // setup recyclerview
        vm.setupRV(binding.recylerView)
        vm.setupLiveData(this)
    }
}
