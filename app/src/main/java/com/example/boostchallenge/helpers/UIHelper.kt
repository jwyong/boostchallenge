package com.example.boostchallenge.helpers

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.Nullable
import com.example.boostchallenge.R
import com.google.android.material.textfield.TextInputLayout

class UIHelper {
    var inputText: String? = null

    private val validateHelper = ValidateHelper()

    // popup with 2 btns
    fun dialog2btn(
            context: Context,
            message: Int,
            @Nullable positiveText: Int? = null,
            @Nullable negativeText: Int? = null,
            positiveFunc: Runnable,
            @Nullable negativeFunc: Runnable? = null,
            isCancellable: Boolean = true
    ): Dialog {

        val dialog = Dialog(context)
        val dialogLayout = LayoutInflater.from(context)
        val dialogView = dialogLayout.inflate(R.layout.dialog2btn, null)

        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(dialogView)
        dialog.setCancelable(isCancellable)
        dialog.create()

        // bind items
        val dialogMsg = dialog.findViewById<TextView>(R.id.dialog_msg)
        val dialogInput = dialog.findViewById<EditText>(R.id.et_input)
        val dialogTIL = dialog.findViewById<TextInputLayout>(R.id.til_input)
        val dialogNegative = dialog.findViewById<TextView>(R.id.negative_button)
        val dialogPositive = dialog.findViewById<TextView>(R.id.positive_button)

        // autofocus
        dialogInput.requestFocus()
        dialog.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)

        // set dialog msg
        dialogMsg.text = context.getString(message)

        //btn negative set function
        if (negativeText != null) {
            dialogNegative.text = context.getString(negativeText)
        }

        //btn positive set function
        if (positiveText != null) {
            dialogPositive.text = context.getString(positiveText)
        }

        //set OnclickListener
        dialogNegative.setOnClickListener {
            negativeFunc?.run()
            dialog.dismiss()
        }

        dialogPositive.setOnClickListener {
            // validate first
            inputText = dialogInput.text.toString()
            if (validateHelper.validateEmpty(inputText)) {
                // if not empty, hide error and run func
                dialogTIL.error = null

                positiveFunc.run()

                dialog.dismiss()

            } else {
                // if empty, show error
                dialogTIL.error = context.getString(R.string.validate_empty_error)
            }
        }

        dialog.show()

        return dialog
    }
}