package com.example.boostchallenge.helpers

object GlobalVars {
    const val TAG1 = "JAY"

    // intent consts
    const val EX_TOPIC_TITLE = "EX_TOPIC_TITLE"
    const val EX_TOPIC_VOTES = "EX_TOPIC_VOTES"
}
