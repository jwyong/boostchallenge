package com.example.boostchallenge.helpers

class ValidateHelper {
    fun validateEmpty(str: String?): Boolean {
        return !str.isNullOrBlank()
    }
}