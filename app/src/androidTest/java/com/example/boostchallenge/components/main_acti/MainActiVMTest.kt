package com.example.boostchallenge.components.main_acti

import android.util.Log
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.example.boostchallenge.helpers.GlobalVars
import com.example.boostchallenge.model.TopicModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Test

import org.junit.Assert.*
import javax.inject.Scope

class MainActiVMTest {
    private val vm = MainActiVM()

    //=== up down btns clicked simulation
    @Test
    fun down_btn_clicked() {
        upDownBtnClickedTest(0)
    }

    fun up_btn_clicked() {
        upDownBtnClickedTest(1)
    }

    private fun upDownBtnClickedTest(type: Int) {
        // add 1 item to list first
        val item = TopicModel(topicTitle = "test topic", topicVotes = 4)
        vm.topicListLD.postValue(mutableListOf(item))

        // simulate up btn clicked from ui thread
        GlobalScope.launch(Dispatchers.Main) {
            vm.upDownVoteBtn(type = type, item = item)

            // assert that item vote increased/decreased by 1
            if (type == 0) {
                assertTrue(vm.topicListLD.value?.first()?.topicVotes == 3)
            } else {
                assertTrue(vm.topicListLD.value?.first()?.topicVotes == 5)
            }
        }
    }
}