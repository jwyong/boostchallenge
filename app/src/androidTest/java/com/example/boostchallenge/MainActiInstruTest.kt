package com.example.boostchallenge

import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction

import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.example.boostchallenge.components.main_acti.MainActi
import com.example.boostchallenge.components.main_acti.MainActiAdapter
import org.hamcrest.Matcher

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Rule
import kotlin.random.Random

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
@LargeTest
class MainActiInstruTest {
    @get:Rule
    var activityRule: ActivityTestRule<MainActi> = ActivityTestRule(MainActi::class.java)
//
//    @Before
//    fun initValidString() {
//        // Specify a valid string.
//        stringToBetyped = "Espresso"
//    }

    @Test
    fun testAddMoreThan20() {
        step1Add20Items()

        // upvote few items first
        step2ScrollAndVote(false)

        // randomly up/downvote few items
        step2ScrollAndVote(true)

        step3AddMoreItems()

        // pause 30 secs for viewing
        pauseEspresso(30000)
    }

    //===== funcs for steps
    private fun step1Add20Items() {
        // add first 20 items first
        for (i in 0..19) {
            addItemToList(i, 1)
        }

        // pause 3 secs for viewing
        pauseEspresso(3000)
    }

    private fun step2ScrollAndVote(isRandom: Boolean) {
        // scroll and upvote/downvote random rows x 5
        for (i in 0..4) {
            scrollToItemAndUpDownVote(isRandom)
        }

        // pause 1 secs for viewing
        pauseEspresso(1000)

        // scroll to top
        getVIByID(R.id.recyler_view).perform(
            RecyclerViewActions.scrollToPosition<MainActiAdapter.Holder>(
                0
            )
        )

        // pause 2 secs for viewing
        pauseEspresso(2000)
    }

    private fun step3AddMoreItems() {
        // add few more items
        for (i in 0..4) {
            addItemToList(i, 2)
        }

        // scroll to btm
        getVIByID(R.id.recyler_view).perform(
            RecyclerViewActions.scrollToPosition<MainActiAdapter.Holder>(
                19
            )
        )
    }

    //===== individual funcs

    private fun addItemToList(i: Int, type: Int) {
        // click add topic btn
        getVIByID(R.id.fab).perform(click())

        // for second batch
        val index: Int
        if (type == 2) {
            index = i + 20
        } else {
            index = i
        }

        if (index != 0) {
            // fill in topic title
            getVIByID(R.id.et_input).perform(typeText("Topic ${index + 1}"))

        } else { // test empty title for first item
            // fill in topic title
            getVIByID(R.id.et_input).perform(typeText("      "))

            // click create
            getVIByID(R.id.positive_button).perform(click())

            // pause for viewing
            pauseEspresso(1000)

            // fill in topic title
            getVIByID(R.id.et_input).perform(typeText("Topic ${index + 1}"))
        }

        // click create
        getVIByID(R.id.positive_button).perform(click())

        // pause for 2 secs (for viewing - first item only)
        if (index == 0) {
            pauseEspresso(2000)
        }
    }

    private fun scrollToItemAndUpDownVote(isRandom: Boolean) {
        // get random index
        val position = Random.nextInt(0, 19)

        // scroll to position
        getVIByID(R.id.recyler_view).perform(
            RecyclerViewActions.scrollToPosition<MainActiAdapter.Holder>(
                position
            )
        )

        // pause for viewing
        pauseEspresso(1000)

        // random upvote or downvote
        var btnChoice = if (isRandom) {
            Random.nextInt(0, 1)
        } else {
            1
        }

        btnChoice = if (btnChoice == 0) {
            R.id.btn_down
        } else {
            R.id.btn_up
        }
        getVIByID(R.id.recyler_view).perform(
            RecyclerViewActions.actionOnItemAtPosition<MainActiAdapter.Holder>(
                position,
                clickChildViewWithId(btnChoice)
            )
        )

        // pause for viewing
        pauseEspresso(3000)

    }

    private fun clickChildViewWithId(id: Int): ViewAction {
        return object : ViewAction {
            override fun getConstraints(): Matcher<View>? {
                return null
            }

            override fun getDescription(): String {
                return "Click on a child view with specified id."
            }

            override fun perform(uiController: UiController, view: View) {
                val v: View = view.findViewById(id)
                v.performClick()
            }
        }
    }


    private fun getVIByID(id: Int): ViewInteraction {
        return onView(withId(id))
    }

    private fun pauseEspresso(timeInMillis: Long) {
        try {
            Thread.sleep(timeInMillis)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}
