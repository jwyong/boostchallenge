package com.example.boostchallenge.helpers

import org.junit.Test

import org.junit.Assert.*

class ValidateHelperTest {
    private val validateHelper = ValidateHelper()

    @Test
    fun validate_multispace() {
        assertFalse(validateHelper.validateEmpty("  "))
    }

    @Test
    fun validate_no_space() {
        assertFalse(validateHelper.validateEmpty(""))
    }

    @Test
    fun validate_with_char() {
        assertTrue(validateHelper.validateEmpty("asdfasdf"))
    }
}