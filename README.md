# BoostChallenge

Android app for boost challenge in Kotlin

1. list of topics stored in mutable list tied to live data in view model (my attempt at mvvm archi)
2. list used to fulfill sorting and limiting function as required
3. created custom ListAdapter to handle DIFF_UTIL bug for lists of same size (additional r&d required)
4. simple unit tests for simple topic title validation and up/down vote btns function
5. simple instrumental test for adding 20 items to list, randomly up/downvoting, and scrolling for viewing.